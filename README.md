### Installation
- `git clone git@gitlab.com:alrib/cliris-front.git`
- `yarn install`
- Create a .env file and set this variable to : `REACT_APP_BASE_URL=http://localhost:4000/`
- `yarn start`

- Staging environment : https://cliris-staging-front.herokuapp.com/
- Prod environment : https://cliris-prod-front.herokuapp.com/
