import './App.css';
import Villes from './components/Villes';

function App() {
  return (
    <div className="App">
      <Villes></Villes>
    </div>
  );
}

export default App;
