import React from "react";

export const Villes = ({villes}) => {

  return (
    <div>
      <h3>Villes : </h3>
      <ul>
        {villes.map((ville) =>
              <li>
                {ville.ville_nom} 
              </li>
        )}
      </ul>
    </div>
  );
};