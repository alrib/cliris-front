import React, { useState, useEffect } from "react";
import Axios from "axios";
import { Filter } from "./Filter";
import { Villes } from "./Villes";

export default function Search() {
  const [word, setWord] = useState("");
  const [villes, setVilles] = useState([]);
  useEffect(() => {
    const getVilles = async () => {
      const result = await Axios.get("http://localhost:4000/api/villes");
      console.log(result)
      setVilles(result.data);
    };

    getVilles();
  }, []);
  const [filterDisplay, setFilterDisplay] = useState([]);

  const handleChange = (e) => {
    setWord(e);
    const oldList = villes.map((ville) => {
      return {
        name: ville.ville_nom.toLowerCase(),
      };
    });

    if (word !== "") {
      let newList = [];

      newList = oldList.filter(
        (user) =>
          user.name.includes(word.toLowerCase()) ||
          user.surname.includes(word.toLowerCase())
      );

      setFilterDisplay(newList);
    } else {
      setFilterDisplay(villes);
    }
  };

  return (
    <div>
      <Filter value={word} handleChange={(e) => handleChange(e.target.value)} />
    </div>
  );
}