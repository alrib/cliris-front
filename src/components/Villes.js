import React, { useState, useEffect } from "react";
import Axios from "axios";

export default function Villes() {
    const [villes, setVilles] = useState([]);
    const [skip, setSkip] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [departement, setDepartement] = useState(0);
    const [population, setPopulation] = useState(1999);
    const [sort, setSort] = useState('');
    const [ville, setVille] = useState('');


    const fetchVilles = async (skip, departement, sort, population, ville) => {
        Axios.get(process.env.REACT_APP_BASE_URL + 'api/villes', {
            params: {limit: 20, page: skip, departement, sort, popSorte: population, ville}
        }).then((response) => {
            setTotalPages(response.data.data.totalPages)
            setVilles(response.data.data.villes)
        })
    }
        
    const nextPage = () => {
        if (skip < totalPages - 1) {
            setSkip(skip + 1)
        }
    }

    const previousPage = () => {
        if (skip > 0) {
            setSkip(skip - 1)
        }
    }


    const sortAsc = () => {
        setSort('ASC')
    }

    const sortDesc = () => {
        setSort('DESC')
    }

    const clearState = () => {
        setVille('')
        setDepartement('')
        setPopulation('')
        setSort('')
        setSkip(0)
    }

    useEffect(() => {
        fetchVilles(skip, departement, sort, population, ville)
    }, [skip, departement, sort, population, ville])
    
    
    return (
    <div> 
        <table style={{"border":"4px", 'borderColor':"black", 'borderStyle':'solid'}}> 
        <tbody>
            <tr>
            <td> ID </td>
            <td> Département </td>
            <td> Slug </td>
            <td> Nom </td>
            <td> Nom simple </td>
            <td> Nom réel </td>
            <td> Nom soundex </td>
            <td> Nom métaphone </td>
            <td> Code postal </td>
            <td> Commune </td>
            <td> Code commune </td>
            <td> Ville arrondissement </td>
            <td> Canton </td>
            <td> AMDI </td>
            <td> Population 2010</td>
            <td> Popultation 1999 </td>
            <td> Population 2012 </td>
            <td> Densité 2010 </td>
            <td> surface </td>
            <td> Longitude (degré) </td>
            <td> Latitude (degré) </td>
            <td> Longitude (grd) </td>
            <td> Latitude (grd) </td>
            <td> Longitude (dms) </td>
            <td> Latitude (dms) </td>
            <td> ZMIN </td>
            <td> ZMAX </td>    
            </tr>
            { 
                villes.map(ville => 
                    <tr style={{"border":"4px", 'borderColor':"black", 'borderStyle':'solid'}}> 
                        <td> { ville.ville_id } </td>
                        <td> { ville.ville_departement } </td>
                        <td> { ville.ville_slug } </td>
                        <td> { ville.ville_nom } </td>
                        <td> { ville.ville_nom_simple } </td>
                        <td> { ville.ville_nom_reel } </td>
                        <td> { ville.ville_nom_soundex } </td>
                        <td> { ville.ville_nom_metaphone } </td>
                        <td> { ville.ville_code_postal } </td>
                        <td> { ville.ville_commune } </td>
                        <td> { ville.ville_code_commune } </td>
                        <td> { ville.ville_arrondissement } </td>
                        <td> { ville.ville_canton } </td>
                        <td> { ville.ville_amdi } </td>
                        <td> { ville.ville_population_2010 } </td>
                        <td> { ville.ville_population_1999 } </td>
                        <td> { ville.ville_population_2012 } </td>
                        <td> { ville.ville_densite_2010 } </td>
                        <td> { ville.ville_surface } </td>
                        <td> { ville.ville_longitude_deg } </td>
                        <td> { ville.ville_latitude_deg } </td>
                        <td> { ville.ville_longitude_grd } </td>
                        <td> { ville.ville_latitude_grd } </td>
                        <td> { ville.ville_longitude_dms } </td>
                        <td> { ville.ville_latitude_dms } </td>
                        <td> { ville.ville_zmin } </td>
                        <td> { ville.ville_zmax } </td>
                    </tr>
                )
            }
            </tbody>
        </table>
        <div> 
            <button onClick={previousPage}>Page précédente</button>
            <button onClick={nextPage}> Page suivante </button><br/><br/>
            <input type="text" placeholder="Population par année" name="name" onChange={e =>setPopulation(e.target.value)}/> 
            <button onClick={sortAsc}> Population asc </button> 
            <button onClick={sortDesc}> Population desc </button> <br/><br/>
            <input type="text" placeholder="Département" name="name" onChange={e =>setDepartement(e.target.value)}/> <br/><br/>
            <input type="text" placeholder="Ville" name="name" onChange={e =>setVille(e.target.value)}/> <br/><br/>
            <button onClick={clearState}> Reset filtre </button> 
        </div>
    </div>)
}